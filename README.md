# Farm

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.1.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

Install npm-run-all is necessary to run the package:build
    - npm install npm-run-all@^4.1.5

Run packages "global-modal and global-snackbar"
    - npm run packages:build
    - include this command in ci/cd
    - can also include the "packages:build" in the ci/cd flow


## Test Directiion
    ng build --prod might not work
    by default admin username exist, there should be error popup



