import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GlobalSnackbarComponent } from './global-snackbar.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatIconModule } from '@angular/material';

@NgModule({
  declarations: [GlobalSnackbarComponent],
  imports: [
    CommonModule,
    MatSnackBarModule,
    MatIconModule,
  ],
  exports: [GlobalSnackbarComponent],
  entryComponents: [GlobalSnackbarComponent],
})
export class GlobalSnackbarModule { }
