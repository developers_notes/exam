import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './core/guard/auth-guard.service';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { LoadingPageComponent } from './pages/general/loading-page/loading-page.component';
import { PermissionGuard } from './shared/models/permission-guard';


const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [/*AuthenticationGuard*/]
  },
  {
    path: 'register',
    component: RegisterComponent,
    canActivate: [/*AuthenticationGuard*/]
  },
  {
    path: 'loading',
    component: LoadingPageComponent,
    // canActivate: [AuthGuard]
  },
  {
    path: 'farm',
    loadChildren: 'src/app/pages/farm/farm.module#FarmModule',
    //canActivate: [AuthGuard],
    //data: {roles: ['view_farm']}
  },
  { path: '**', redirectTo: '/login', pathMatch: 'full' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true, onSameUrlNavigation: 'reload' })],
  providers: [],
  exports: [RouterModule]
})
export class AppRoutingModule { }
