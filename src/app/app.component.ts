import { Component, OnInit } from '@angular/core';
import { GlobalSnackbarService } from './../../projects/global-snackbar/src/lib/global-snackbar.service';
import { BooleanQuestionComponent } from './shared/modals/boolean-question/boolean-question.component';
import { ModalService } from './shared/services/modal.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: []

})

export class AppComponent implements OnInit{


  public holder: boolean = false;
  constructor(
    private readonly snackBar: GlobalSnackbarService,
    private readonly modalService: ModalService,
  ) {}
  // title = 'farm';

  showSnackBar(): void {
    this.holder = !this.holder;
    if (this.holder) {
      // this.snackBar.openError('this is the error');
    } else {
      // this.snackBar.openSuccess('This is a success');
    }
  } 

  ngOnInit() {
    //this.auth.configAuth();
  }

  async showModal(): Promise<any> {
    const config = {
      width: 'auto',
      height: '10rem',
      data: {
        title: ``,
        yes: `Yes`,
        no: `No`,  
        component: BooleanQuestionComponent
      }
    };
    const ret = await this.modalService.open(config, false);
    if (ret === 1) {
      console.log('its a yes');
    } else if (ret === 2) {
      console.log('its a no');
    }
  }
}
