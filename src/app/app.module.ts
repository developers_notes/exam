import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { SharedModule } from './shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HeaderInterceptor } from './core/http/header-interceptor';
import { GlobalModalModule } from 'global-modal';
import { GlobalSnackbarModule } from 'global-snackbar';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatIconModule } from '@angular/material/icon';
import { LoginComponent } from './pages/login/login.component';
import { LoadingPageComponent } from './pages/general/loading-page/loading-page.component';
import { AuthGuard } from './core/guard/auth-guard.service';
import { RegisterComponent } from './pages/register/register.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LoadingPageComponent,
    RegisterComponent,
  ],
  imports: [
    MatIconModule,
    BrowserModule,
    SharedModule,
    HttpClientModule,
    BrowserAnimationsModule,
    GlobalModalModule,
    GlobalSnackbarModule,
    AppRoutingModule,
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: HeaderInterceptor, multi: true},
    AuthGuard,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
