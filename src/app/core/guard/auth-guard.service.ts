import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { UserSessionModel } from 'src/app/shared/models/user.model';
/**
 * This guard disables routes if no valid access token has been set
 */
@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private router: Router,
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    // if there is a token in auth guard or a token in the URL parameters, allow this route
    const expectedRoles = route.data.roles as Array<string>;
    const users: UserSessionModel = JSON.parse( sessionStorage.getItem('user') ) || null;
    if (users === null) {
      return false;
    }
    let ret;
    // use filter
    expectedRoles.forEach( (val: any, index) => {
      ret = users.realm_access.roles.find( x => 
        x === val
      );
    });
    console.log('addfarm? ', ret);
    if (ret) {
      return true;
    }
    this.router.navigate([403]);
    return false;
  }
}
