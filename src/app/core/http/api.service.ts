import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { ErrorGetModel } from '../../shared/models/api-error.model';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { CommonService } from 'src/app/shared/services/common.service';
import { GlobalSnackbarService } from './../../../../projects/global-snackbar/src/lib/global-snackbar.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private url: string;

  constructor(
    private readonly http: HttpClient,
    private readonly router: Router,
    private readonly commonService: CommonService,
    private readonly snackBar: GlobalSnackbarService,
  ) {
    this.url = environment.url;
  }

  get<T>(param: string): Observable<T> {
    const url = param ? (`${this.url}${param}`) : this.url;
    return (this.http.get(url)).pipe(
      catchError( (err: ErrorGetModel) => {
        this.gotoError(err);
        throw new Object({status: err.status, statusText: err.statusText, error: err.error});
      }),
    ) as any;
  }

  delete<T>(id: string): Observable<T> {
    return (this.http.delete(`${this.url}/${id}`)).pipe(
      catchError( (err: ErrorGetModel) => {
        this.gotoError(err);
        throw new Object({status: err.status, statusText: err.statusText, error: err.error});
      }),
    ) as any;
  }

  post<T>(url: string, body?: any): Observable<T> {
    //return (this.http.post(`${this.url}${url}`, body)).pipe(
    return (this.http.post(`${url}`, body)).pipe(
      catchError( (err: ErrorGetModel) => {
        this.gotoError(err);
        throw new Object({status: err.status, statusText: err.statusText, error: err.error});
      }),
    ) as any;
  }

  put<T>(url: string, body?: any): Observable<T> {
    return (this.http.put(`${this.url}${url}`, body)).pipe(
      catchError( (err: ErrorGetModel) => {
        this.gotoError(err);
        throw new Object({status: err.status, statusText: err.statusText, error: err.error});
      }),
    ) as any;
  }

  gotoError(err: ErrorGetModel) {
    console.log('gotoError', err.error);
    if (err.error[0].type && (err.error[0].type === 'EXPIRED_TOKEN' ||
      err.error[0].type === 'INVALID_TOKEN' ||
      err.error[0].type === 'MISMATCH_TOKEN' ||
      err.error[0].type === 'NOTFOUND_TOKEN'
    )) {
        // this.snackBar.openError(err.error[0].message);
        this.commonService.clearLocalStorage();
        this.router.navigate(['/login']);
    }
  }
}
