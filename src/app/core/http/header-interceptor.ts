import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { HttpEvent, HttpInterceptor, HttpResponse, HttpHandler, HttpRequest,} from '@angular/common/http';
export class HeaderInterceptor implements HttpInterceptor {
  constructor() {}
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const profile = JSON.parse(sessionStorage.getItem('user')) || {};
    // Clone the request to add the new header
    const clonedRequest = req.clone({ headers: req.headers.set('Authorization', `Bearer ${profile.accessToken}`) });

    return next.handle(clonedRequest).pipe(
      tap((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          //this.spinnerService.hide();
        } else {
          //this.spinnerService.hide();
        }
        }, (error) => {
          //this.spinnerService.hide();
        })
      );
    }
  }