import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import * as io from 'socket.io-client/dist/socket.io';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  socket: any;

  constructor() {
    this.socket = io(environment.apisocket);
  }

  listen(eventname: string) {
    return new Observable((subscriber) => {
      this.socket.on(eventname, (data) => {
        subscriber.next(data);
      });
    });
  }

  emit(eventname, data): Promise<any> {
    return new Promise ( (resolve, reject) => {
      try {
        this.socket.emit(eventname, data);
        resolve(true);
      } catch (e) {
        reject(e);
      }
    });
  }
}
