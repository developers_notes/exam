import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FarmIndexModel } from 'src/app/shared/models/farm.model';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FarmService {

  constructor(private http: HttpClient) {}

  listFarms(id = null): Observable<any> {
    const { publicApi } = environment;
    if (id === null) {
      return this.http.get(`${publicApi}/farm`)
    } else {
      return this.http.get(`${publicApi}/farm/${id}`)
    }
    
  }

  createFarm(data) {
    const { publicApi } = environment;
    return this.http.post(`${publicApi}/farm`, data)
  }
  
  updateFarm(id, data) {
    const { publicApi } = environment;
    return this.http.put(`${publicApi}/farm/${id}`, data)
  }

  deleteFarm(id) {
    const { publicApi } = environment;
    return this.http.delete(`${publicApi}/farm/${id}`)
  }
}
