import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalSnackbarService } from 'global-snackbar';
import { ApiService } from 'src/app/core/http/api.service';
import { FarmService } from 'src/app/core/services/farm.service';
import { LocationModel, LocationProvinceModel, LocationRegionModel, LocationCityModel, LocationBgyModel }
from 'src/app/shared/models/location.model';


@Component({
  selector: 'app-farm-detail',
  templateUrl: './farm-detail.component.html',
  styleUrls: ['./farm-detail.component.scss']
})
export class FarmDetailComponent implements OnInit {
  public id: any;
  public type: string = 'Add';
  public form: FormGroup;
  public d: {};
  public location: LocationModel = {
    barangays: [],
    cities: [],
    regions: [],
    provinces: []
  };
  private agriculture_props: any;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly fb: FormBuilder,
    private readonly http: ApiService,
    private readonly snackbar: GlobalSnackbarService,
    private readonly router: Router,
    private farmService: FarmService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id'] ? params['id'] : 0;
      this.type = params['type'] ? params['type'] : '';
    });
    this.location.regions = JSON.parse(sessionStorage.getItem('regions'));
    this.agriculture_props = JSON.parse(sessionStorage.getItem('agriculture_props'));

    console.log('this.agriculture_props: ', this.agriculture_props);
    console.log('this.agriculture_props.cultivation: ', this.agriculture_props.cultivation);

    this.d = {
      cultivationMethods: this.agriculture_props.cultivation,
      landTenure: this.agriculture_props.land, 
      cropsGrown: this.agriculture_props.cropsgrown,
      livestock: this.agriculture_props.livestock,
      irrigation: this.agriculture_props.irrigation,
      soilClassification: this.agriculture_props.soilClassification,
      certifications: [
        {label: 'Good Agricultural Practice', value: 'gap'},
        {label: 'OCCP', value: 'occp'},
        {label: 'NICERT', value: 'nicert'},
        {label: 'PGS', value: 'pgs'},
        {label: 'HACCP', value: 'sanhaccpdy'},
        {label: 'Rainforest Alliance', value: 'ra'},
      ],
      
    }

    this.form = this.fb.group({
      name: [''],
      longitude: new FormControl({value: 0, disabled: false}),
      latitude: new FormControl({value: 0, disabled: false}),
      region: [''],
      province: [''],
      cityMunicipality: [''],
      baranggay: [''],
      cultivationMethods: [''],
      landTenure: [''],
      farmSize: [1],
      cropsGrown: new FormControl([]),
      livestock: new FormControl([]),
      irrigation: ['1'],
      soilClassification: [''],
      cropInsurance: ['1'],
      registrySystemForBasicSectorsInAgriculture: ['1'],
      governmentProjectSupport: [1],
      trainingCertifications: [''],
      certifications: new FormControl([]),
      link: [''],
      img: ['']
      
    });

    if (this.type === 'edit') {
      this.getSpecificFarm();
    }
  }

  getLoc(type: string = 'province') {
    
    if (type === 'province') {
      if ( this.form.get('region').value ) {
        this.location.provinces = [];
        this.form.get('province').setValue('');
        this.form.get('cityMunicipality').setValue('');
        this.form.get('baranggay').setValue('');
        this.location.provinces = JSON.parse(sessionStorage.getItem('provinces')).filter( (ret: LocationProvinceModel) =>
          ret.regCode === this.form.get('region').value.regCode
        );
      }
    } else if (type === 'city') {
      if ( this.form.get('province').value ) {
        this.location.cities = [];
        this.form.get('cityMunicipality').setValue('');
        this.form.get('baranggay').setValue('');
        this.location.cities = JSON.parse(sessionStorage.getItem('cities')).filter( (ret: LocationCityModel) =>
          ret.provCode === this.form.get('province').value.provCode
        );
      }
    } else if (type === 'bgy') {
      if ( this.form.get('cityMunicipality').value ) {
        this.location.barangays = [];
        this.form.get('baranggay').setValue('');
        this.location.barangays = JSON.parse(sessionStorage.getItem('barangays')).filter( (ret: LocationCityModel) =>
          ret.citymunCode === this.form.get('cityMunicipality').value.citymunCode
        );
      }
    } else {
      return [];
    }
  }

  async getSpecificFarm() {
    try {
      const ret = await this.farmService.listFarms(this.id).toPromise();
      this.form.get('name').setValue(ret.name);
      this.form.get('longitude').setValue(ret.location.geoCoord.longitude);
      this.form.get('latitude').setValue(ret.location.geoCoord.latitude);
      this.form.get('region').setValue(ret.location.region);
      this.form.get('cultivationMethods').setValue(ret.cultivationMethods[0]);
      this.form.get('province').setValue(ret.location.province);
      this.form.get('cityMunicipality').setValue(ret.location.cityMunicipality);
      this.form.get('baranggay').setValue(ret.location.baranggay);
      this.form.get('landTenure').setValue(ret.landTenure);
      this.form.get('farmSize').setValue(ret.farmSize);
      this.form.get('soilClassification').setValue(ret.soilClassification);
      if (ret.cropsGrown) {
        this.form.get('cropsGrown').setValue(ret.cropsGrown);
      }
      if (ret.livestock) {
        this.form.get('livestock').setValue(ret.livestock);
      }
      if (ret.certifications) {
        this.form.get('certifications').setValue(ret.certifications);
      }
      this.form.get('irrigation').setValue(ret.irrigation);
      this.form.get('trainingCertifications').setValue(ret.trainingCertifications);
    } catch(e) {
      this.snackbar.openError('There is an error catching this farm');
    }
     
  }

  async save(update = true) {
    let msg = 'Success creating farm';
    try {
      const payload = {
        name: this.form.get('name').value,
        location: {
          baranggay: this.form.get('baranggay').value,
          cityMunicipality: this.form.get('cityMunicipality').value,
          geoCoord: {
            latitude: this.form.get('latitude').value,
            longitude: this.form.get('longitude').value,
          },
          province: this.form.get('province').value,
          region: this.form.get('region').value,
        },
        governmentProjectSupport: this.form.get('name').value,
        cultivationMethods: [this.form.get('cultivationMethods').value],
        farmSize: this.form.get('farmSize').value,
        cropsGrown: this.form.get('cropsGrown').value,
        certifications: this.form.get('certifications').value,
        livestock: this.form.get('livestock').value,
        irrigation: this.form.get('irrigation').value,
        soilClassification: this.form.get('soilClassification').value,
        landTenure: this.form.get('landTenure').value,
        trainingCertifications: this.form.get('trainingCertifications').value,
      }

      if (update === true) {
        await this.farmService.updateFarm(this.id, payload).toPromise();  
        this.snackbar.openSuccess('Success updating farm');
      } else {
        await this.farmService.createFarm(payload).toPromise();  
      }
      this.snackbar.openSuccess(msg);
      this.router.navigate(['/farm']);
    } catch(e) {
      console.log(e);
      this.snackbar.openError(e.error.message || 'There is an error saving farm');
    }
    
  }

  async create() {
    try {
      const payload = {
        id: Date.now(),
        name: this.form.get('name').value,
        longitude: this.form.get('longitude').value,
        latitude: this.form.get('latitude').value,
        status: this.form.get('status').value,
      }

      await this.farmService.createFarm(this.form.getRawValue());  
    } catch(e) {
      console.log(`error:`, e);
    }
  }

  uploadFileEvt($event) {
    const reader = new FileReader();
    const img = $event.target.files[0];
    reader.readAsDataURL(img);
    reader.onload = () => {
      this.form.controls.img.setValue(reader.result);
    };
  }

}
