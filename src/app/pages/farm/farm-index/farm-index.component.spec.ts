import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmIndexComponent } from './farm-index.component';

describe('FarmtIndexComponent', () => {
  let component: FarmIndexComponent;
  let fixture: ComponentFixture<FarmIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FarmIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
