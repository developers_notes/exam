import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BooleanQuestionComponent } from 'src/app/shared/modals/boolean-question/boolean-question.component';
import { FarmIndexModel } from 'src/app/shared/models/farm.model';
import { ColsTableModel, TableActionModel } from 'src/app/shared/models/general.model';
import { ModalService } from 'src/app/shared/services/modal.service';
import { GlobalSnackbarService } from '../../../../../projects/global-snackbar/src/lib/global-snackbar.service';

import { ApiService } from 'src/app/core/http/api.service';
import { FarmService } from 'src/app/core/services/farm.service';
//import { randomLocation } from 'random-location';

@Component({
  selector: 'app-farm-index',
  templateUrl: './farm-index.component.html',
  styleUrls: ['./farm-index.component.scss']
})
export class FarmIndexComponent implements OnInit {

  public dataSource: FarmIndexModel[];
  public cols: ColsTableModel[] = [];
  public actions: TableActionModel = {
    copy: false,
    edit: true,
    delete: true,
    history: false,
    order: false,
  };
  private loc = [];

  constructor(
    private readonly router: Router,
    private readonly modalService: ModalService,
    private readonly snackBar: GlobalSnackbarService,
    private farmService: FarmService,
    private readonly http: ApiService,
  ) { }

  ngOnInit() {
    this.dataSource = [];
    this.getFarm();
    this.cols = [
      { name: '_id', label: 'ID' },
      { name: 'name', label: 'Name' },
      { name: 'province', label: 'Province' },
      { name: 'long', label: 'Longitude' },
      { name: 'lat', label: 'Latitude' },
    ];
    
  }

  async getFarm() {
    this.dataSource = [];
    const res: [] = await this.farmService.listFarms().toPromise();
    this.dataSource = res.map( (ret: any) => {
      return {
        ...ret, 
        long: ret.location.geoCoord.longitude,
        lat: ret.location.geoCoord.latitude,
        province: ret.location.province,
      };
    })
  }

  clickAction(event) {
    if (event.action === 'edit') {
      this.router.navigate([`farm/detail/edit/${event.data._id}`]);
    } else if (event.action === 'delete') {
      this.showModal(event.data);
    }
  }

  async showModal(data: any): Promise<any> {
    const config = {
      width: 'auto',
      height: '11rem',
      data: {
        buttons: false,
        title: `Are you sure you want to delete ${data.name}`,
        yes: `Yes`,
        no: `No`,  
        component: BooleanQuestionComponent
      }
    };
    const ret = await this.modalService.open(config, false);
    if (ret === 1) {
      await this.farmService.deleteFarm(data._id).toPromise();
      this.getFarm();
    }
  }

}
