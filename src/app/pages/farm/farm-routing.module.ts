import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/app/core/guard/auth-guard.service';
import { OutletComponent } from 'src/app/shared/component/outlet/outlet.component';
import { FarmDetailComponent } from './farm-detail/farm-detail.component';
import { FarmIndexComponent } from './farm-index/farm-index.component';


const routes: Routes = [
  {
    path: '', 
    component: OutletComponent,
    children: [
      {
        path: '',
        component: FarmIndexComponent,
        data: { roles: [] },
        //canActivate: [AuthenticationGuard, RoleGuard]
      },
      {
        path: 'create',
        component: FarmDetailComponent,
        canActivate: [AuthGuard],
        data: {roles: ['add_farm']}
      },
      {
          path: 'detail/:type/:id',
          component: FarmDetailComponent,
          canActivate: [AuthGuard],
          data: {roles: ['edit_farm']}
      },
      {
        path: 'detail/:type',
        component: FarmDetailComponent,
    },
    ]
  },
  {
    path: '**',
    redirectTo: '/farm',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FarmRoutingModule { }
