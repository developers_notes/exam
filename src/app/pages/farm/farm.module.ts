import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FarmRoutingModule } from './farm-routing.module';
import { FarmIndexComponent } from './farm-index/farm-index.component';
import { FarmDetailComponent } from './farm-detail/farm-detail.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FarmRoutingModule,
  ],
  declarations: [FarmIndexComponent,
    FarmDetailComponent
  ],
  
})
export class FarmModule { }
