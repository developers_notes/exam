import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { UserSessionModel } from 'src/app/shared/models/user.model';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/core/http/api.service';
import {  BehaviorSubject, Subscription } from 'rxjs';
import { SocketService } from 'src/app/core/http/socket.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { ModalService } from 'src/app/shared/services/modal.service';
import { GlobalSnackbarService } from 'global-snackbar';
import { LoginModalComponent } from 'src/app/shared/modals/login-modal/login-modal.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit, OnDestroy {
  private readonly apisocket: string = environment.apisocket;
  private subscriber: Subscription;
  private returnSocket: BehaviorSubject<any> = new BehaviorSubject('');
  public form: FormGroup;
  public showSpinner: boolean = false;

  constructor(
    private readonly router: Router,
    private readonly http: ApiService,
    private readonly fb: FormBuilder,
    private readonly socket: SocketService,
    private readonly commonService: CommonService,
    private readonly modalService: ModalService,
    private readonly snackbar: GlobalSnackbarService
  ) {
  }

  ngOnInit() {

    this.form = this.fb.group({
      username: new FormControl({value: 'johndoe', disabled: false}),
      password: new FormControl({value: 'somePassword', disabled: false}),
    });
    this.listener();
  }

  ngOnDestroy() {
    this.subscriber.unsubscribe();
  }

  listener() {
    this.subscriber = this.socket.listen('process').subscribe( (ret: any)  => {
      this.showSpinner = false;
      console.log('retFromServer: ', ret);
      if (ret.error) {
        this.snackbar.openError(ret.desc);
      } else {
        console.log('retFromServer: ', ret);
        const config = {
          width: 'auto',
          height: '20rem',
          disableClose : true,
          data: {
            salt: ret.salt,
            username: this.form.get('username').value,
            yes: `Login`,
            no: false,
            component: LoginModalComponent
          }
        };
        this.modalService.open(config, false).then( (r: any) => {
          this.snackbar.openSuccess(r);
        });
      }

    });
  }

  logout() {
    sessionStorage.clear();
  }

  async login() {
    this.showSpinner = true;
    //const newPass = this.commonService.custom_hash(this.form.get('password').value, this.form.get('username').value);

    // Get Login salt first to be needed in login
    let payload: any = {
      command: 'loginSalt',
      username: this.form.get('username').value,
    };
    await this.socket.emit('process', payload);

    /*this.returnSocket.subscribe (ret => {
      console.log('ret: ', ret);
      const challenge = this.commonService.custom_hash(ret.salt, ret.savedPassword);
      // Try to login
      payload = {
        command: 'login',
        username: this.form.get('username').value,
        challenge,
      };

      this.socket.emit('process', payload);
      console.log('challenge: ', payload); 

      this.showSpinner = false;
    });*/

  }

  getErrorMessage() {
    return this.form.controls[`username`].hasError('required') ? 'You must enter a value' :
        this.form.controls[`username`].hasError('pattern') ? 'Not a valid username' :
        this.form.controls[`username`].hasError('custom') ? 'No err0r but error' :
        this.form.controls[`username`].hasError('minlength') ? 'Required length is at least 3 characters' :
           '';
  }

  register() {
    this.router.navigate([`/register`]);
  }


}
