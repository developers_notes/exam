import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/core/http/api.service';
import { SocketService } from 'src/app/core/http/socket.service';
import { Router } from '@angular/router';
import { Subscriber, Subscription } from 'rxjs';
import { CommonService } from 'src/app/shared/services/common.service';
import { GlobalSnackbarService } from 'global-snackbar';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RegisterComponent implements OnInit, OnDestroy {

  public form: FormGroup;
  public showSpinner: boolean = false;
  private listener: Subscription;

  constructor(
    private readonly fb: FormBuilder,
    private readonly http: ApiService,
    private readonly socket: SocketService,
    private readonly router: Router,
    private readonly commonService: CommonService,
    private readonly snackbar: GlobalSnackbarService
  ) { }

  ngOnDestroy() {
    this.listener.unsubscribe();
  }

  ngOnInit() {
    this.form = this.fb.group({
      username: new FormControl({value: 'johndoe', disabled: false}, Validators.required),
      email: new FormControl({value: 'test@test.com', disabled: false}, Validators.required),
      password: new FormControl({value: 'somePassword', disabled: false}, Validators.required),
      displayname: new FormControl({value: 'John', disabled: false}, Validators.required),
    });

    this.listener = this.socket.listen('process').subscribe( (ret: any)  => {
      this.showSpinner = false;
      if (ret.error) {
        this.snackbar.openError(ret.desc);
      } else {
        this.snackbar.openSuccess('Success Registration');
        this.router.navigate(['/login']);
      }
    });
  }

  async register() {
    this.showSpinner = true;
    const newPass = this.commonService.custom_hash(this.form.get('password').value, this.form.get('username').value);
    const payload: any = {
      command: 'register',
      username: this.form.get('username').value,
      displayname: this.form.get('displayname').value,
      email: this.form.get('email').value,
      hashedPassword: newPass
    };
    this.socket.emit('process', payload);
  }

  login() {
    this.router.navigate([`/login`]);
  }

  getErrorMessage() {
    return this.form.controls[`username`].hasError('required') ? 'You must enter a value' :
        this.form.controls[`username`].hasError('pattern') ? 'Not a valid username' :
        this.form.controls[`username`].hasError('custom') ? 'No err0r but error' :
        this.form.controls[`username`].hasError('minlength') ? 'Required length is at least 3 characters' :
           '';
  }

}
