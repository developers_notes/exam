import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  constructor(
    // private readonly keycloackService: KeycloakService,
  ) { }

  ngOnInit() {
    // this.user = JSON.parse( localStorage.getItem('user') );
  }

  goTo(link: string) {
    window.location.href = link;
  }

  logout() {
    // this.keycloackService.logout( environment.root_url );
  }

}
