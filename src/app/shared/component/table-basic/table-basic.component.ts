import { Component, Input, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { MatPaginator, MatSort } from '@angular/material';

@Component({
  selector: 'app-table-basic',
  templateUrl: './table-basic.component.html',
  styleUrls: ['./table-basic.component.scss']
})
export class TableBasicComponent implements OnInit {
  
  @Input() dataSource: MatTableDataSource<any>;
  @Input() cols: any[] = [];
  @Input() actions: any[] = [];
  @Output() clickAction = new EventEmitter();
  public displayedColumns: any[];

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor() { }

  ngOnInit() {
    this.displayedColumns = this.cols.map( ret => {
      return ret.name;
    });
    this.displayedColumns.push('actions');
  }

  onClick(action: string, data: any) {
    this.clickAction.emit({ action, data });
  }

}
