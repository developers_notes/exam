import { Component, OnInit } from '@angular/core';
import { ModalService } from '../../services/modal.service';
import { EmptyComponent } from '../../modals/empty/empty.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  public user;

  constructor(
    private readonly modalService: ModalService,
    private readonly router: Router,
  ) { }

  ngOnInit() {
    const name = JSON.parse( sessionStorage.getItem('user') ) || {};
    this.user = name.given_name + ' ' + name.family_name;
  }

  async settingmodal() {
    const config = {
      width: '20rem',
      height: '15rem',
      data: {
        title: `Log out? `,
        subtitle: 'asa',
        buttons: {
          yes: 'Yes',
          no: 'No',
        },
        position: 'center',
        component: EmptyComponent
      }
    };
    const ret = await this.modalService.open(config);
    if (ret === 'confirm') {
      sessionStorage.clear();
      this.router.navigate(['/login']);
    }
  }

}
