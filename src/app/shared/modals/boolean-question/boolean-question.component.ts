import { Component, Inject, OnInit, Optional, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-boolean-question',
  templateUrl: './boolean-question.component.html',
  styleUrls: ['./boolean-question.component.scss']
})
export class BooleanQuestionComponent implements OnInit {

  constructor(
    private readonly dialog: MatDialogRef<BooleanQuestionComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }

  ngOnInit() {
  }

  closeButton(val: any = false) {
    this.dialog.close(val);
  }

}
