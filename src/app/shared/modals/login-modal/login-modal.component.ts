import { Component, Inject, OnDestroy, OnInit, Optional, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { SocketService } from 'src/app/core/http/socket.service';
import { CommonService } from 'src/app/shared/services/common.service';

@Component({
  selector: 'app-login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LoginModalComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  private subscriber: Subscription;

  constructor(
    private readonly dialog: MatDialogRef<LoginModalComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    private readonly fb: FormBuilder,
    private readonly socket: SocketService,
    private readonly commonService: CommonService,
    private readonly router: Router,
  ) { }

  ngOnDestroy() {
    this.subscriber.unsubscribe();
  }

  ngOnInit() {

    this.form = this.fb.group({
      password: new FormControl({value: 'somePassword', disabled: false}),
    });

    this.subscriber = this.socket.listen('process').subscribe( (ret: any)  => {
      if (ret === true) {
        setTimeout(() => {
          this.dialog.close(true);
          window.location.href = '/#/farm';
        }, 1000);
      } else {

      }
    });
  }

  closeButton(val: any = false) {
    if (val === 1) {

      const newPass = this.commonService.custom_hash(this.form.get('password').value, this.data.username);
      const challenge = this.commonService.custom_hash(this.data.salt, newPass);
      const payload: any = {
        command: 'login',
        username: this.data.username,
        password: this.form.get('password').value,
        challenge,
      };
      this.socket.emit('process', payload);
    }
  }

}
