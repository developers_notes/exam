export interface CustomError {
    code: number;
    type: string;
    message: string;
}

export interface ErrorGetModel {
    status: number;
    statusText: string;
    error: CustomError[];
}
