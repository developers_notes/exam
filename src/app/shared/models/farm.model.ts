export interface FarmIndexModel {
    id: number;
    name: string;
    status: string;
}

export interface FarmDetailModel {
    id?: string;
    name: string;
    status: string;
}
