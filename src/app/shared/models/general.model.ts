export interface ColsTableModel {
    name: string;
    label: string;
}

export interface TableActionModel {
    copy: boolean;
    edit: boolean;
    delete: boolean;
    history?: boolean;
    order?: boolean;
}
