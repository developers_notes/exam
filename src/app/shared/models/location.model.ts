export interface LocationModel {
    provinces: LocationProvinceModel[];
    regions: LocationRegionModel[];
    cities: LocationCityModel[];
    barangays: LocationBgyModel[];
}

export interface LocationProvinceModel {
    id?: string;
    psgcCode: string;
    provDesc: string;
    regCode: string;
    provCode: string;
}

export interface LocationRegionModel {
    id?: string;
    psgcCode: string;
    regDesc: string;
    regCode: string;
}

export interface LocationCityModel {
    id?: string;
    psgcCode: string;
    citymunDesc: string;
    regCode: string;
    provCode: string;
    citymunCode: string;
}

export interface LocationBgyModel {
    id?: string;
    brgyCode: string;
    brgyDesc: string;
    regCode: string;
    provCode: string;
    citymunCode: string;
}
