export interface UserLoginModel {
    token: string;
    user: any;
    logo?: string;
    name?: string;
    role: number;
}

export interface UserSessionModel {
    email: string;
    email_verified: boolean;
    exp: number;
    family_name: string;
    given_name: string;
    name: string;
    preferred_username: string;
    realm_access: RolesModel;
    azp: string; // this is the clientId
    resource_access: AccessModel;
}

interface RolesModel {
    roles: [];
}

interface AccessModel {
    account: RolesModel;
}
