import { Injectable } from '@angular/core';
import jsSHA from 'jssha';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(
  ) { }

  clearLocalStorage() {
    sessionStorage.clear();
  }

  getCurrentYearMonth() {
    const d = new Date();
    const currentY = d.getFullYear();
    let currentM: any = d.getMonth() + 1;
    if (currentM < 10) {
        currentM = '0' + currentM;
    }
    return {y: currentY, m: currentM};
  }

  custom_hash(v1, v2) {
    if (!v1 || !v2) {
      return false;
    }
    const shaObj = new jsSHA('SHA-256', 'TEXT');
    shaObj.setHMACKey(v1, 'TEXT');
    shaObj.update(v2);
    return shaObj.getHash('HEX');
  }
}
