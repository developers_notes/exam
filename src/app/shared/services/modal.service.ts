import { Injectable } from '@angular/core';
import { GlobalModalComponent } from './../../../../projects/global-modal/src/lib/global-modal.component';
import { MatDialog } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor(
    private readonly dialog: MatDialog,
  ) { }

  open(config, disableclose : boolean = true): Promise<any> {
    return new Promise ((resolve) => {
      this.dialog
      .open(GlobalModalComponent, { ...config, disableclose})
      .afterClosed()
      .subscribe(async data => {
        resolve(data);
      });
    });
  }
}
