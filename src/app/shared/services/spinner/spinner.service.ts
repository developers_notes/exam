import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SpinnerService {

  visibility: BehaviorSubject<boolean>;
 
  constructor() {
    this.visibility = new BehaviorSubject(false);
  }
 
  show() {
    setTimeout(() => { // added timeout so that ExpressionChangedAfterItHasBeenCheckedError will be fixed
      this.visibility.next(true);
    });
  }
 
  hide() {
    this.visibility.next(false);
  }
}
