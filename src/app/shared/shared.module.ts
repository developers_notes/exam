
import { NgModule } from '@angular/core';
import { MaterialModule } from '../core/material/material.module';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BooleanQuestionComponent } from './modals/boolean-question/boolean-question.component';
import { OutletComponent } from './component/outlet/outlet.component';
import { SidebarComponent } from './component/sidebar/sidebar.component';
import { TableBasicComponent } from './component/table-basic/table-basic.component';
import { CommonModule } from '@angular/common';
import { ToolbarComponent } from './component/toolbar/toolbar.component';
import { EmptyComponent } from './modals/empty/empty.component';
import { GlobalModalComponent } from '../../../projects/global-modal/src/lib/global-modal.component';
import { LoginModalComponent } from './modals/login-modal/login-modal.component';

//outlet and sidebar also here

/** Custom Modules */

@NgModule({
    imports: [
        RouterModule,
        MaterialModule,
        CommonModule,
        FormsModule, ReactiveFormsModule,
    ],
    exports: [
        TableBasicComponent,
        MaterialModule,
        FormsModule, ReactiveFormsModule,
    ],
    providers: [],
    declarations: [
        BooleanQuestionComponent,
        OutletComponent,
        SidebarComponent,
        TableBasicComponent,
        ToolbarComponent,
        EmptyComponent,
        GlobalModalComponent,
        LoginModalComponent,
    ],

    entryComponents: [
        BooleanQuestionComponent,
        TableBasicComponent,
        EmptyComponent,
        GlobalModalComponent,
        LoginModalComponent
    ],
    bootstrap: []
  })
  export class SharedModule {}
