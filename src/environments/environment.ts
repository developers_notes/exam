// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const authCodeFlowConfig = {
  url: 'https://192.168.99.100:8443/auth',
  realm: 'admin-client',
  clientId: 'farm-admin',
};

const authConf = {
  discovery:
    "https://192.168.99.100:8443/auth/realms/admin-client/.well-known/openid-configuration",
  issuer:
    "https://192.168.99.100:8443/auth/realms/admin-client",
  base: "http://https://192.168.99.100:8443/auth",
  realmName: "admin-client",
  redirectUri: "http://localhost:4200",
  scope: "openid email profile",
  clientId: "farm-admin",
  requireHttps: false,
  oidc: true,
  logoutUrl:
    "https://192.168.99.100:8443/auth/realms/admin-client/protocol/openid-connect/logout"
};

export const environment = {
  production: false,
  root_url: 'http://localhost:4200',
  baseUrl: 'http://localhost:4200',
  url: 'http://localhost:4200',
  publicApi: 'http://localhost:3000',
  ssoConfig: authCodeFlowConfig,
  authConf,
  agriculture_props: 'aksyon_agrekultura.json',
  secret: 'superSecretKey',
  apisocket: 'ws://localhost:4000',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
